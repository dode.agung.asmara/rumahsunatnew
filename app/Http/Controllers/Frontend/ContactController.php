<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Contact\SendContactRequest;
use App\Mail\Frontend\Contact\SendContact;
use Illuminate\Support\Facades\Mail;
use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;

/**
 * Class ContactController.
 */
class ContactController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

    public function seo()
    {
        $title = 'Kontak - Rumah Sunat Bali';
        $description = 'Layanan sunat(sirkumsisi) modern pertama di Bali, untuk usia bayi, anak hingga dewasa. Rumah sunat bali hadir di denpasar dan badung. Sunat ditangani dokter berpengalaman dengan metode sunat modern tanpa jahitan, (sunat klem, dan sunat stapler)';
        $url = url('/').'/kontak';
        $image = url('/frontend/assets/images/logo.png');

        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', 'homepage');

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::addImage($image);
    }

    public function index()
    {
        $this->seo();
        $name = 'Rumah Sunat Bali';
        $description = 'Layanan sunat(sirkumsisi) modern pertama di Bali, untuk usia bayi, anak hingga dewasa. Rumah sunat bali hadir di denpasar dan badung. Sunat ditangani dokter berpengalaman dengan metode sunat modern tanpa jahitan, (sunat klem, dan sunat stapler)';
        $telephone = '+6287777114800';
        $context = \JsonLd\Context::create('local_business', [
                'name' => $name,
                'description' => $description,
                'telephone' => $telephone,
                'openingHours' => 'sun,mon,tue,wed,thu,fri,sat',
                'image' => [
                    url('/frontend/images/blog/post/default.png')
                ],
                  "priceRange"=>"$",
                'address' => [
                    'streetAddress' => 'Jl. Tukad Batanghari No.42, Panjer, Kec. Denpasar Bar., Kota Denpasar, Bali 80225',
                    'addressLocality' => 'Denpasar, Bali',
                    'addressRegion' => 'Denpasar',
                    'postalCode' => '80225',
                ],
                'geo' => [
                    'latitude' => '115.23022679341761',
                    'longitude' => '-8.680559463306608',
                ],
        ]);
        return view('frontend.contact',['jsondata'=>$context]);
    }

    /**
     * @param SendContactRequest $request
     *
     * @return mixed
     */
    public function send(SendContactRequest $request)
    {
        Mail::send(new SendContact($request));

        return redirect()->back()->withFlashSuccess(__('alerts.frontend.contact.sent'));
    }
}
