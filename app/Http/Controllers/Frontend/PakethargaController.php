<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;

class PakethargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function seo()
    {
        $title = 'Paket Harga - Rumah Sunat Bali';
        $description = 'Rumah Sunat Bali - Layanan Sunat Anak, Remaja, & Dewasa';
        $url = url('/').'/paketharga';
        $image = url('/frontend/assets/images/logo.png');

        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', 'homepage');

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::addImage($image);
    }

    public function index()
    {
        $this->seo();

        $name = 'Rumah Sunat Bali';
        $description = 'Layanan sunat(sirkumsisi) modern pertama di Bali, untuk usia bayi, anak hingga dewasa. Rumah sunat bali hadir di denpasar dan badung. Sunat ditangani dokter berpengalaman dengan metode sunat modern tanpa jahitan, (sunat klem, dan sunat stapler)';
        $telephone = '+6287777114800';
        $context = \JsonLd\Context::create('local_business', [
                'name' => $name,
                'description' => $description,
                'telephone' => $telephone,
                'openingHours' => 'sun,mon,tue,wed,thu,fri,sat',
                'image' => [
                    url('/frontend/images/blog/post/default.png')
                ],
                 "priceRange"=>"$",
                'address' => [
                    'streetAddress' => 'Jl. Tukad Batanghari No.42, Panjer, Kec. Denpasar Bar., Kota Denpasar, Bali 80225',
                    'addressLocality' => 'Denpasar, Bali',
                    'addressRegion' => 'Denpasar',
                    'postalCode' => '80225',
                ],
                'geo' => [
                    'latitude' => '115.23022679341761',
                    'longitude' => '-8.680559463306608',
                ],
        ]);
        return view('frontend.paketharga',['jsondata'=>$context]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
