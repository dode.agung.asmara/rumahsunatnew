@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!!$jsondata->generate()??''!!}
<meta charset="utf-8">
<meta name="description" content="@yield('meta_description', 'Rumah Sunat Bali')">
<meta name="author" content="@yield('meta_author', 'GeneralWeb')">
<!-- Stylesheets -->
<link href="{{ url('frontend/assets/')}}/css/bootstrap.css" rel="stylesheet">
<link href="{{ url('frontend/assets/')}}/css/revolution-slider.css" rel="stylesheet">
<link href="{{ url('frontend/assets/')}}/css/style.css" rel="stylesheet">
<link href="{{ url('frontend/assets/')}}/css/jquery-ui.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="{{ url('frontend/assets/')}}/images/faviconrumahsunat.ico" type="image/x-icon">
<link rel="icon" href="{{ url('frontend/assets/')}}/images/faviconrumahsunat.ico" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link href="{{ url('frontend/assets/')}}/css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="{{ url('frontend/assets/')}}/js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
    <!-- Main Header-->
    <header class="main-header">
        
        <!-- Header Top -->
        <div class="header-top">
            <div class="auto-container clearfix">
                <!--Top Left-->
                <div class="top-left pull-left">
                    <ul class="links-nav clearfix">
                        <li><span class="icon fa fa-envelope-o"></span><a href="mail:info@rumahsunatbali.com">info@rumahsunatbali.com</a></li>
                        <li><span class="icon fa fa-phone"></span><a href="tel:087777114800">Call Us Now : 0877 7711 4800</a></li>
                    </ul>
                </div>
                
                <!--Top Right-->
                <div class="top-right pull-right">
                    <div class="top-left pull-left">
                    <ul class="links-nav clearfix">
                        <li class="language dropdown">
                            @if(App::isLocale('en'))
                                <a class="btn btn-default dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">En <span class="icon fa fa-caret-down"></span></a>
                            @else
                                 <a class="btn btn-default dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">Id <span class="icon fa fa-caret-down"></span></a>
                            @endif     
                            <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu2">
                                <li ><a href="{{url('/lang/en')}}">English</a></li>
                                <li class="selected"><a href="{{url('/lang/id')}}">Indonesia</a></li> 
                            </ul>
                        </li>
                        <li><span class="icon fa fa-clock-o"></span><a href="#">WE ARE OPEN! : Everyday 9:00-20:00</a></li>
                    </ul>
                </div>
                </div>
            </div>
        </div>
        <!-- Header Top End -->
        
        <!--Header-Upper-->
        <div class="header-upper">
            <div class="auto-container">
                    
                <div class="logo-outer">
                    <div class="logo"><a href="{{url('/')}}"><img src="{{ url('frontend/assets/')}}/images/logo.webp" alt="" title=""></a></div>
                </div>
                    
                <div class="nav-outer clearfix">
                
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <!--Left Nav-->
                            <ul class="navigation left-nav clearfix">
                                <li class="{{ Request::is('/metode-sunat')? 'current':'' }}"><a href="{{ url('/')}}/metode-sunat">@lang('buttons.frontend.header.metode')</a></li>
                                <li class="{{ Request::is('/layanan-sunat')? 'current':'' }}"><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.header.service')</a></li>
                                <li class="{{ Request::is('/review')? 'current':'' }}"><a href="{{ url('/review')}}">@lang('buttons.frontend.header.testimony')</a>
                                </li>
                            </ul>
                            <!--Right Nav-->
                            <ul class="navigation right-nav clearfix">
                                <li class="{{ Request::is('blogs')? 'current':'' }}"><a href="{{ url('blogs')}}">@lang('buttons.frontend.header.blog')</a></li>
                                <li class="{{ Request::is('paket-harga-sunat')? 'current':'' }}"><a href="{{ url('paket-harga-sunat')}}">@lang('buttons.frontend.header.paket')</a>
                                <li class="{{ Request::is('kontak-sunat')? 'current':'' }}"><a href="{{ url('kontak-sunat')}}">@lang('buttons.frontend.header.kontak')</a></li>
                            </ul>
                        </div>
                    </nav>
                    
                
                    
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="{{url('/')}}" class="img-responsive"><img src="{{ url('frontend/assets/')}}/images/logo-small.webp" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="{{ Request::is('/')? 'current':'' }}"><a href="{{url('/')}}">@lang('buttons.frontend.header.home')</a>
                                </li>
                                <li class="{{ Request::is ('/layanan-sunat')?  'current':'' }}"><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.header.service')</a></li>
                                <li class="{{ Request::is ('/metode-sunat')?  'current':'' }}"><a href="{{ url('/')}}/metode-sunat">@lang('buttons.frontend.header.metode')</a></li>
                                <li class="{{ Request::is ('review')?  'current':'' }}"><a href="{{ url('/review')}}">@lang('buttons.frontend.header.testimony')</a>
                                <li class="{{ Request::is('blogs')? 'current':'' }}"><a href="{{ url('blogs')}}">@lang('buttons.frontend.header.blog')</a></li>
                                <li class="{{ Request::is('paket-harga-sunat')? 'current':'' }}"><a href="{{ url('paket-harga-sunat')}}">@lang('buttons.frontend.header.paket')</a>
                                </li>
                                <li class="{{ Request::is('kontak-sunat')? 'current':'' }}"><a href="{{ url('kontak-sunat')}}">@lang('buttons.frontend.header.kontak')</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->
    
    <div id="appVue">
        @yield('content')
    </div> 

    <!--Involved Section-->
    <section class="involved-section">
        <div class="auto-container">
            <div class="involved-inner wow fadeInUp" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-3.webp);">
                <div class="row clearfix">
                    <div class="column col-md-8 col-sm-12 col-xs-12">
                        <h2>@lang('content.section.layanan.cta')</h2>
                    </div>
                    <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                        <a href="{{ url('contact')}}" class="theme-btn btn-style-one">@lang('content.section.layanan.kontak')</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Involved Section-->

    <!--Main Footer-->
    <footer class="main-footer" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-2.webp);">
        <div class="auto-container">
            <!--widgets section-->
            <div class="widgets-section">
                <div class="row clearfix">
                
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget logo-widget">
                                    <div class="footer-logo">
                                        <a href="{{ url('/')}}"><img src="{{ url('frontend/assets/')}}/images/logo-2.webp" alt=""></a>
                                    </div>
                                    <div class="widget-content">
                                        <div class="text">@lang('content.section.footer.text')</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <!--Links Widget-->
                                <div class="footer-widget links-widget">
                                    <div class="footer-title">
                                        <h2>@lang('content.section.layanan.layanan')</h2>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.footer.list.sunatbalita')</a></li>
                                            <li><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.footer.list.sunatanak')</a></li>
                                            <li><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.footer.list.sunatremaja')</a></li>
                                            <li><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.footer.list.sunatdewasa')</a></li>
                                            <li><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.footer.list.sunatgemuk')</a></li>
                                            <li><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.footer.list.sunatkhusus')</a></li>
                                            <li><a href="{{ url('/')}}/layanan-sunat">@lang('buttons.frontend.footer.list.homecare')</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        
                            <!--Footer Column-->
                            <div class="footer-column col-md-12 col-sm-12 col-xs-12">
                                <!--Links Widget-->
                                <div class="footer-widget links-widget">
                                    <div class="footer-title">
                                        <h2>@lang('content.section.layanan.kontak')</h2>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li>(0877) 7711 4800</li>
                                            <li>info@rumahsunatbali.com</li>
                                            <li>Denpasar, Bali</li>
                                        </ul>
                                        <br>
                                        <ul class="social-links-two">
                                            <li class="facebook"><a href="https://www.facebook.com/sunatbali"><span class="fa fa-facebook"></span></a></li>
                                            <li class="google-plus"><a href="https://www.instagram.com/rumahsunatbali/"><span class="fa fa-instagram"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
      
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <!--Footer Bottom-->
            <div class="footer-bottom">
                <div class="row clearfix">
                    <div class="column col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright">&copy; Copyrights 2020 Rumah Sunat Bali. All Rights Reserved</div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </footer>
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="{{ url('frontend/assets/')}}/js/jquery.js"></script>
<script src="{{ url('frontend/assets/')}}/js/bootstrap.min.js"></script>
<script src="{{ url('frontend/assets/')}}/js/revolution.min.js"></script>
<script src="{{ url('frontend/assets/')}}/js/jquery.fancybox.pack.js"></script>
<script src="{{ url('frontend/assets/')}}/js/jquery.fancybox-media.js"></script>
<script src="{{ url('frontend/assets/')}}/js/owl.js"></script>
<script src="{{ url('frontend/assets/')}}/js/appear.js"></script>
<script src="{{ url('frontend/assets/')}}/js/jquery-ui.js"></script>
<script src="{{ url('frontend/assets/')}}/js/wow.js"></script>
<script src="{{ url('frontend/assets/')}}/js/script.js"></script>
<!-- WhatsHelp.io widget -->
<script type="text/javascript" async>
    (function () {
        var options = {
            whatsapp: "+6287777114800", // WhatsApp number 
            email: "sunatuntuksemua@gmail.com", 
            call: "+6287777114800", // Call phone number
            company_logo_url: "http://rumahsunatbali.com/theme_costume/images/logo-img.webp", // URL of company logo (png, jpg, gif)
            greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.", // Text of greeting message
            call_to_action: "Ask Our Team", // Call to action
            button_color: "#007995", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "whatsapp,call" // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->

        <!-- Scripts -->
    <!-- @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!} -->
    @stack('after-scripts')
    @yield('pagespecificscripts')
</body>
</html>