@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

<!--Page Title-->
    <section class="page-title" style="background-image:url({{ url('frontend/assets/')}}/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h3>@lang('content.paket.text')</h3>
                <ul class="bread-crumb">
                    <li><a href="{{url('/paketharga')}}">Home</a></li>
                    <li>@lang('content.paket.text')</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

<section id="layanan">
    <section class="welcome-section" >
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <figure class="image">
                    <img src="{{ url('frontend/assets/')}}/images/resource/fasilitaskami.webp" alt="">
                </figure>
                <!-- <div class="title">Our Weekly Classes</div>
                <div class="text">We are group of teachers who really love childrens and enjoy every moment of teaching</div> -->
            </div>
            <div class="row clearfix">
            
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/BAYI.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- div class="post-date">Feb 29, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatbayianak.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatbayianak.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/REMAJA.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Marc 17, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatremaja.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatremaja.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/DEWASA.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Apr 01, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatdewasa.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatdewasa.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="row clearfix">
            
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/GEMUK.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- div class="post-date">Feb 29, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatgemuk.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatgemuk.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/KHUSUS.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Marc 17, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatkhusus.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatkhusus.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/HOMESERVICE.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Apr 01, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.homecare.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.homecare.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</section>

@endsection