@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

<!--Page Title-->
    <section class="page-title" style="background-image:url({{ url('frontend/assets/')}}/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h1>@lang('content.paket.text')</h1>
                <ul class="bread-crumb">
                    <li><a href="{{url('/paketharga')}}">Home</a></li>
                    <li>@lang('content.paket.text')</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

<!--Welcome Section-->
    <section class="welcome-section">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered">
            	<br>
                 <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images\icons\sec-title-icon-1.png" alt=""></div>
            	<h2>@lang('content.paket.welcome.head')</h2>
                <div class="text">@lang('content.paket.welcome.text')
                </div>
                
                <div class="row clearfix">
                    <div class="content-side col-lg-6 col-md-6 col-sm-12 col-xs-12">

                      <ul class="accordion-box">
                            <li class="accordian-title">@lang('content.paket.text')</li>
                            <!--Block-->
                            <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon fa fa-arrow-circle-right"></span> </div>@lang('content.paket.qna.one.question')</div>
                                <div class="acc-content current">
                                    <div class="content">
                                       @lang('content.paket.qna.one.answer')
                                    </div>
                                </div>
                            </li>
                            
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-arrow-circle-right"></span> </div>@lang('content.paket.qna.two.question')</div>
                                <div class="acc-content">
                                    <div class="content">
                                        @lang('content.paket.qna.two.answer')
                                    </div>
                                </div>
                            </li>
                            
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-arrow-circle-right"></span> </div>@lang('content.paket.qna.three.question')</div>
                                <div class="acc-content">
                                    <div class="content">
                                        @lang('content.paket.qna.three.answer')
                                        
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="sidebar-side col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <figure class="image">
                            <img src="{{ url('frontend/assets/')}}/images\paketharga.webp" alt="">
                        </figure>
                    </div>

                </div>

            </div>
            
              <div class="sec-title centered">
                <BR>
                <div class="text">@lang('content.paket.closingparagraph')
                </div>
            </div>
            
        </div>
    </section>
    <!--End Welcome Section-->

@endsection