@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    
    <!--Main Slider-->
<section id="home">
    <section class="main-slider" data-start-height="800" data-slide-overlay="yes">
        
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.one')" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.one')" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">                     
                    </li>

                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.two')" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.two')" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">                     
                    </li>

                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.three')" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.three')" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">                     
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.four')" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="{{ url('frontend/assets/')}}/images/main-slider/@lang('content.slider.four')" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                                  
                    </li>

                    
                    
                </ul>
                
            </div>
        </div>
        
    </section>
</section>
    <!--End Main Slider-->

<section class="featured-section style-two">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images/icons/sec-title-icon-1.webp" alt=""></div>
                <h2>@lang('content.section.welcome.head')</h2>
                <div class="title">@lang('content.section.welcome.title')</div>
                <div class="text">@lang('content.section.welcome.text').</div>
            </div>
            <!--Image-->
            <figure class="image">
               <center><img src="{{ url('frontend/assets/')}}/images/resource/welcome-img.webp" alt=""></center>
            </figure>
        </div>
        <div class="footer-style-two">
            <div class="footer-bg"></div>
        </div>
    </section>



    <!--Featured Section-->
<section id="metode">
    <section class="featured-section style-two">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images/icons/sec-title-icon-1.webp" alt=""></div>
                <h2>@lang('content.section.welcome.metode.judul.head')</h2>
                <div class="text">@lang('content.section.welcome.metode.judul.text')</div>
                
            </div>
            <!--End Sec Title-->
            <div class="row clearfix">
                <!--Column/ Pull Left-->
                <div class="column pull-left col-md-4 col-sm-6 col-xs-12">
                    <!--Feature Block-->
                    <div class="feature-block">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images/metode/konvensional.webp" alt="">
                            </div>
                            <h3><a>@lang('content.section.welcome.metode.conventional.head')</a></h3>
                            <div class="text">@lang('content.section.welcome.metode.conventional.text')</div>
                        </div>
                    </div>
                    
                    <!--Feature Block-->
                    <div class="feature-block">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images/metode/klam.webp" alt="">
                            </div>
                            <h3><a>@lang('content.section.welcome.metode.clamp.head')</a></h3>
                            <div class="text">@lang('content.section.welcome.metode.clamp.text')</div>
                        </div>
                    </div>
                    
                </div>
                <!--Column / Pull Right-->
                <div class="column pull-right col-md-4 col-sm-6 col-xs-12">
                    <!--Feature Block Two-->
                    <div class="feature-block-two">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images/metode/ring.webp" alt="">
                            </div>
                            <h3><a>@lang('content.section.welcome.metode.bipolar.head')</a></h3>
                            <div class="text">@lang('content.section.welcome.metode.bipolar.text')</div>
                        </div>
                    </div>
                    
                    
                    <!--Feature Block Two-->
                    <div class="feature-block-two">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images/metode/stapler.webp" alt="">
                            </div>
                            <h3><a>@lang('content.section.welcome.metode.stapler.head')</a></h3>
                            <div class="text">@lang('content.section.welcome.metode.stapler.text')</div>
                        </div>
                    </div>
                    
                </div>
            
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                    <figure class="image wow fadeInUp">
                        <img src="{{ url('frontend/assets/')}}/images/resource/metodekami1.webp" alt="">
                    </figure>
                </div>
                
            </div>
            
        </div>
    </section>
</section>
    <!--End Featured Section-->
    
    <!--Join Section-->

    <section class="join-section" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-1.webp);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="column col-md-9 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <h2>@lang('content.section.cta.head')</h2>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                            <div class="text">@lang('content.section.cta.text')</div>
                        </div>
                    </div>
                </div>
                <!--Column-->
                <div class="btn-column col-md-3 col-sm-12 col-xs-12">
                    <a href="{{ url('contact')}}" class="theme-btn btn-style-one">@lang('content.section.cta.button')</a>
                </div>
            </div>
        </div>
    </section>
    <!--End Join Section-->
<section id="layanan">
    <section class="welcome-section" >
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <figure class="image">
                    <img src="{{ url('frontend/assets/')}}/images/resource/fasilitaskami.webp" alt="">
                </figure>
                <!-- <div class="title">Our Weekly Classes</div>
                <div class="text">We are group of teachers who really love childrens and enjoy every moment of teaching</div> -->
            </div>
            <div class="row clearfix">
            
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/BAYI.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- div class="post-date">Feb 29, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatbayianak.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatbayianak.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/REMAJA.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Marc 17, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatremaja.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatremaja.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/DEWASA.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Apr 01, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatdewasa.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatdewasa.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="row clearfix">
            
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/GEMUK.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- div class="post-date">Feb 29, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatgemuk.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatgemuk.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/KHUSUS.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Marc 17, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.sunatkhusus.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.sunatkhusus.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images/pelayanan/HOMESERVICE.webp" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Apr 01, 2017</div> -->
                                <h3><a>@lang('content.section.layanan.option.homecare.head')</a></h3>
                                <div class="text">@lang('content.section.layanan.option.homecare.text')</div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</section>

    <!--Call To Action-->
    <section class="call-to-action" style="background-image:url({{ url('frontend/assets/')}}/images/background/2.webp);">
        <div class="auto-container">
            <div class="row clearfix">
            
                <!--Content Column-->
                <div class="content-column pull-right col-md-8 col-sm-12 col-xs-12">
                    <h3>@lang('content.section.ctacall.head')</h3>
                    <!-- <div class="text">We are group of teachers who really love childrens and eaching and playing with our students.enjoy every and playing with our students.</div> -->
                    <a href="{{ url('kontak')}}" class="theme-btn btn-style-one">@lang('content.section.ctacall.button')</a>
                </div>
                <!--Image Column-->
                <div class="image-column pull-left col-md-4 col-sm-12 col-xs-12">
                    <figure class="image wow fadeInUp">
                        <img src="{{ url('frontend/assets/')}}/images/resource/baby.webp" alt="">
                    </figure>
                </div>
                
            </div>
            
           
            
        </div>
    </section>
    <!--End Call To Action-->
    
    
    <!--Testimonial Section-->
    <section class="join-section" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-1.webp);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="column col-md-9 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <h2>@lang('content.section.testimony.head')</h2>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                            <div class="text">@lang('content.section.testimony.text')</div>
                        </div>
                    </div>
                </div>
                <!--Column-->
                <div class="btn-column col-md-3 col-sm-12 col-xs-12">
                    <a href="{{ url('/review')}}" class="theme-btn btn-style-one">@lang('content.section.testimony.button')</a>
                </div>
            </div>
        </div>
    </section>
    <!--End Testimonial Section-->
    
    <!--Blog Section-->
<section id="blog">
    <section class="blog-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images/icons/sec-title-icon-1.webp" alt=""></div>
                <h2>blog & Activities</h2>
                <br><br><br>
            </div>
             
            <div class="row clearfix">
                <!--Blog Column-->
               @foreach($blogsdata->chunk(3) as $chunk)
                <div class="blog-column col-md-6 col-sm-12 col-xs-12">
                    <!--News Style Three-->
                    @foreach($chunk as $data)
                    <div class="news-style-three">
                        <div class="row clearfix">
                            <!--Image Column-->
                            <div class="image-column col-md-4 col-sm-4 col-xs-12">
                                <div class="image">
                                    <a href="{{ url('/blog/'.$data->slug)}}"><img src="{{$data->image}}" alt=""></a>
                                </div>
                            </div>
                            <!--Content Column-->
                            <div class="content-column col-md-8 col-sm-8 col-xs-12">
                                <div class="content-inner">
                                    <h3><a href="{{ url('/blog/'.$data->slug)}}">{{$data->title}}</a></h3>
                                    <ul class="post-meta">
                                        <li><a><span class="icon fa fa-user"></span>{{$data->author_name}}</a></li>
                                    </ul>
                                </div>
                                <div class="text">{{$data->summary}}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!--Tab Column-->
               
               @endforeach      
                    
            </div>
           
            </div>
        </div>
    </section>
</section>
    <!--End Blog Section-->
        
@endsection
