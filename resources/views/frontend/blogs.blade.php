@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <section class="page-title" style="background-image:url(frontend/assets/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h1>Blog/Article</h1>
                <ul class="bread-crumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li>Blogs</li>
                </ul>
            </div>
        </div>
    </section>

    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!--Blog-->
                    <section class="blog-classic">
                        
                        
                        <!--News Style Four-->
                      @foreach ($blogsdata as $blog)
                        <div class="news-style-four">
                            <div class="inner-box">
                                <!--Image Column-->
                                <div class="image">
                                    <a href="{{ url('/blog/'.$blog->slug)}}"><img src="{{$blog->image}}" alt="" ></a>
                                </div>
                                <!--Content Column-->
                                <div class="content-column">
                                    <div class="inner">
                                        <div class="post-date">{{$blog->updated_at}}</div>
                                        <h3><a href="{{ url('/blog/'.$blog->slug)}}">{{$blog->title}}</a></h3>
                                        <ul class="post-meta">
                                            <li>by <span>{{$blog->author_name}}</span></li>
                                        </ul>
                                        <div class="text">{{$blog->summary}}</div>
                                       <a class="read-more" href="{{ url('/blog/'.$blog->slug)}}">Read More <span class="icon fa fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      @endforeach 
                        
                    </section>
					{{ $blogsdata->links() }}
                   
                   
                </div>
                
                <!--Sidebar-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        
                        <!--Services Post Widget-->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title">
                                <h3>Latest Feed</h3>
                            </div>
                            <!--Post-->
                            @foreach($alldata as $latestpost)
                            <article class="post">
                                <figure class="post-thumb img-circle">
                                    <a href="{{url('blog/'.$latestpost->slug)}}">
                                        <img width="70px" height="70px" src="{{$latestpost->image}}" alt="">
                                    </a>
                                </figure>
                                <div class="text"><a href="{{url('blog/'.$latestpost->slug)}}">{{$latestpost->title}}</a></div>
                                <div class="post-info">Posted by {{$latestpost->author_name}}</div>
                            </article>
                            @endforeach
                            <!--Post-->
                        </div>
                        
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>>    

@endsection
