@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url({{ url('frontend/assets/')}}/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h1>Kontak</h1>
                <ul class="bread-crumb">
                    <li><a href="index.html">Home</a></li>
                    <li>Kontak</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    <section class="contact-section">
        <div class="auto-container">
            <div class="info-section">
                <div class="row clearfix">
                    <!--Contact Info Block-->
                    <div class="contact-info-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box wow fadeIn" data-wow-delay="0ms">
                                <span class="icon flaticon-location-pin"></span>
                            </div>
                            <div class="text"><b>Lokasi Klinik Renon</b><br>Jalan Tukad Batang Hari no 42. lantai 2. Panjer, Denpasar Selatan, Bali</div>
                        </div>
                    </div>
                    
                    <!--Contact Info Block-->
                    <div class="contact-info-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box wow fadeIn" data-wow-delay="300ms">
                                <span class="icon flaticon-web"></span>
                            </div>
                            <div class="text"><b>Lokasi Klinik Dalung</b><br> Perum Dalung Permai, Jl. Tegal Permai No.88, Kerobokan Kaja, Kuta Utara, Kabupaten Badung</div>
                        </div>
                    </div>
                    
                    <!--Contact Info Block-->
                    <div class="contact-info-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box wow fadeIn" data-wow-delay="600ms">
                                <span class="icon flaticon-smartphone"></span>
                            </div>
                            <b><div class="text">087-777-11-4800 <br> rumahsunatbali@gmail.com</div></b>
                            <br>
                            <div class="widget-content">
                                <ul class="social-links-two">
                                    <li class="facebook"><a href="https://www.facebook.com/sunatbali"><span class="fa fa-facebook"></span></a></li>
                                    <li class="google-plus"><a href="https://www.instagram.com/rumahsunatbali/"><span class="fa fa-instagram"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

     <!--Map Section-->
    <section class="map-section">
        <!--Map Outer-->
        <div class="map-outer">
            <!--Map Canvas-->
            <div class="map-canvas" data-zoom="10" data-lat="-37.817085" data-lng="144.955631" data-type="roadmap" data-hue="#ffc400" data-title="Envato" data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
            </div>
        </div>
    </section>

@endsection

@section('pagespecificscripts')
    
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
    <script src="{{url('frontend/assets')}}/js\map-script.js"></script>

@endsection
<!-- @push('after-scripts')
    @if(config('access.captcha.contact'))
        @captchaScripts
    @endif
@endpush -->