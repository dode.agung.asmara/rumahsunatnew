<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */
    'frontend'=>[
        'header' => [
            'home' => 'Home',
            'metode' => 'Methods',
            'service' => 'Services',
            'testimony' => 'Testimony',
            'blog' => 'Blog',
            'paket' => 'Packages',
            'kontak' => 'Contact',
        ],
        'footer' => [
            'text' => 'Layanan Kami',
            'list' => [
                'sunatbalita' => 'Toddler circumcision ',
                'sunatanak' => 'Child circumcision',
                'sunatremaja' => 'Tenage circumcision',
                'sunatdewasa' => 'Adult circumcision',
                'sunatgemuk' => 'Burried penile circumcision',
                'sunatkhusus' => 'Circumcision with special needs ',
                'homecare' => 'Home Care Service',
            ]
        ]
    ],
   
];
