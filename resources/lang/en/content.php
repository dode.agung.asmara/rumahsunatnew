<?php

return [
    'slider' => [
        'one' => 'image1-en.webp',
        'two' => 'image2-en.webp',
        'three' => 'image3-en.webp',
        'four' => 'image4-en.webp',
    ],
    'section' => [
        'welcome' => [
            'head' => 'Welcome to Rumah Sunat Bali',
            'title' => 'Outpatient circumcision clinic',
            'text' => 'Modern medical outpatient services based on general practitioner practice for circumcision. Our services are highly standardized and managed by doctors who are experienced in circumcision for babies to adults',
            'metode'=>[   
                'judul'=>[
                    'head'=>'Our Circumcision Methods',
                    'text'=>'Below our circumcision methods'
                ],            
                'conventional'=>[
                    'head' =>'Conventional circumcision method',
                    'text' =>'Circumcision by minor surgical methods, with conventional suture wounds ',
                ],
                'clamp'=>[
                    'head' =>'Clamp circumcision method',
                    'text' =>'Circumcision with the clamp method, using a special device that prints the results of the circumcision’s cut . the wound results are neat according to the clamp device used. This method does not use stitches on the wound ',
                ],
                'bipolar'=>[
                    'head' =>'Bipolar Cautery Circumcision Method ',
                    'text' =>'Circumcision with the clamp method, using a special device that prints the results of the circumcision’s cut. This method does not use stitches on the wound ',
                ],
                'stapler'=>[
                    'head' =>'Stapler Circumcision Method',
                    'text' =>'Circumcision with the clamp method, using a special device that prints the results of the circumcision’s cut. The wound results are neat according to the clamp device used. This method does not use stitches on the wound',
                ],

            ]
        ],    



        'testimony' => [
            'head' => 'Our Testimony',
            'text' => 'See how our client talk about us',
            'button' => 'See Testimony',
        ],   

        'cta' => [
            'head' => 'Get Involved!',
            'text' => 'Finding best place to Circumcision',
            'button' => 'Contact Now!',
        ],

        'ctacall' => [
            'head' => '<span>reservation now, </span>How to make an appointment <br> Rumah Sunat Bali?',
            'button' => 'Call now'
        ],
        'footer'=>[
            'text'=>'Rumah Sunat Bali provides professional experience in carrying out the process of Circumcision with services and methods in accordance with medical procedures'
        ],
        'layanan' => [
            'cta'=>'Get to know about Rumah Sunat Bali',
            'layanan'=>'Our Service',
            'kontak'=>'Contact us!',
            'option' => [
                'sunatbayianak'=>[
                    'head' => 'Infant & maximum age 12 years',
                    'text' => 'circumcision in infant and chidhood age, we provide 3 option of methods,  conventional, clamp and stapler ',
                ],
                'sunatremaja'=>[
                    'head' => 'Adolecent',
                    'text' => 'Circumcision in group age 13 to maximum 17 years old we provide 2 option of methods, clamp and stapler',
                ],
                'sunatdewasa'=>[
                    'head' => 'Adult circumcision',
                    'text' => 'Adult circumcision we suggest 2 option of methods, conventional and stapler circumcision',
                ],
                'sunatgemuk'=>[
                    'head' => 'Burried Penie circumcision ',
                    'text' => ' for this special case, burried penile need a further examination and observation from our doctor, the procedure will be discussed accordance to patient\'s condition.',
                ],
                'sunatkhusus'=>[
                    'head' => 'circumcision with special needs',
                    'text' => 'Circumcision Special conditions such as repair results from prior circumcision that are not neat, foreskin is still long, swelling of the foreskin or other abnormality.',
                ],
                'sunatkhusus'=>[
                    'head' => 'circumcision with special needs',
                    'text' => 'Circumcision Special conditions such as repair results from prior circumcision that are not neat, foreskin is still long, swelling of the foreskin or other abnormality.',
                ],
                'homecare'=>[
                    'head' => 'Home Care Service',
                    'text' => 'Home care service for wound treatment.',
                ],
            ]
        ],
    ],
    'paket' => [
        'text'=>'Pricing List',
        'welcome' =>[
            'head'=>'Circumcision',
            'text'=>'All packages for circumcision procedures include doctor\'s consultation, circumcision treatment, medicine and wound care within normal healing ranges'
        ],
        'qna'=>[
            'one'=>[
                'question'=>'Todler (Max 1 year)',
                'answer'=>' <p>Infant & maximum age 12 years: starting IDR 950,000 – IDR 2,500,000</p>'
            ], 
            'two'=>[
                'question'=>'Adolescence 13 - 17 TAHUN',
                'answer'=>'<p>Adolescence 13 years -17 years: from IDR 1,400,000 to IDR 2,750,000</p>'
            ],    
            'three'=>[
                'question'=>'Adult',
                'answer'=>'<p>Adult age: from IDR  2,000,000 to IDR  3,500,000</p>'
            ],    
        ],
        'closingparagraph'=>'Detailed costs will be explained during the consultation according to the method used. Price changes can occur at any time and are explained by our admin during consultation'
    ],
    

   
];
