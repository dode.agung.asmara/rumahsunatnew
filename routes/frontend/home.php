<?php

use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\Blog\BlogController;
use App\Http\Controllers\Frontend\PakethargaController;
use App\Http\Controllers\Frontend\MetodeController;
use App\Http\Controllers\Frontend\LayananController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('review', [HomeController::class, 'review'])->name('review');

Route::get('blogs', [BlogController::class, 'index'])->name('blogs');
Route::get('blogs/all', [BlogController::class, 'all'])->name('blogs.all');
Route::get('blog/{slug}', [BlogController::class, 'article'])->name('blog.article');

Route::get('metode-sunat', [MetodeController::class, 'index'])->name('metode.sunat');
Route::get('layanan-sunat', [LayananController::class, 'index'])->name('layanan.sunat');
Route::get('paket-harga-sunat', [PakethargaController::class, 'index'])->name('paketharga');

Route::get('kontak-sunat', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
